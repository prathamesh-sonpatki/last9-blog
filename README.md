All the [Last9 blog](https://last9.io/blog) articles. [Subscribe](https://last9.io/blog/subscribe) to the blog to get the latest articles in your inbox.

# Blog posts
<!-- BLOG-POST-LIST:START -->
- [1979, a nuclear accident, and SRE](https://last9.io/blog/1979-a-nuclear-accident-and-sre/)
- [Ingest OpenTelemetry metrics with Prometheus natively](https://last9.io/blog/native-support-for-opentelemetry-metrics-in-prometheus/)
- [How to make high cardinality work in time series databases: Part 1](https://last9.io/blog/how-to-make-high-cardinality-work-in-time-series-databases-part-1/)
- [OpenTelemetry for dummies: ELI5](https://last9.io/blog/opentelemetry-for-dummies-eli5/)
- [OpenTelemetry vs. Prometheus](https://last9.io/blog/opentelemetry-vs-prometheus/)
<!-- BLOG-POST-LIST:END -->
- [Influx vs. Prometheus](https://last9.io/blog/prometheus-vs-influxdb/)
- [Prometheus Alternatives](https://last9.io/blog/prometheus-alternatives/)
- [How to restart Kubernetes pods with Kubectl](https://last9.io/blog/how-to-restart-kubernetes-pods-with-kubectl-tutorial/)
- [Prometheus best practices for writing exporters](https://last9.io/blog/best-practices-using-and-writing-prometheus-exporters/)
- [Service Mesh Comparison](https://last9.io/blog/comparing-popular-service-mesh-offerings/)
- [Prometheus vs Cortex](https://last9.io/blog/prometheus-vs-cortex/)
- [What is P95](https://last9.io/blog/your-percentiles-are-incorrect-p99-of-the-times/)
- [What is Prometheus](https://last9.io/blog/what-is-prometheus/)
- [Implementing service level objectives](https://last9.io/blog/a-practical-guide-to-implementing-slos/)
- [Prometheus Cardinality](https://last9.io/blog/how-to-manage-high-cardinality-metrics-in-prometheus/)